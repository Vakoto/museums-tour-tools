package encryption

import "bytes"

func AddPadding(src []byte, size int) []byte {
	return append(src, bytes.Repeat([]byte{0x00}, size)...)
}

func RemovePadding(src []byte, size int) []byte {
	return src[:(len(src) - size)]
}
