package encryption

import (
	"crypto/cipher"
	"encoding/json"
)

func Encrypt(cipher cipher.Block, data interface{}) ([]byte, error) {
	jsonData, err := json.Marshal(data)
	if err != nil {
		return []byte{}, err
	}

	paddingSize := len(jsonData) % cipher.BlockSize()
	if paddingSize != 0 {
		jsonData = AddPadding(jsonData, paddingSize)
	}

	encData := make([]byte, 0)
	encBlock := make([]byte, cipher.BlockSize())

	i := 0
	for i < len(jsonData) {
		cipher.Encrypt(encBlock, jsonData[i:i+cipher.BlockSize()])
		encData = append(encData, encBlock...)
		i += cipher.BlockSize()
	}

	return encData, nil
}

func Decrypt(cipher cipher.Block, encData []byte, data interface{}) error {
	jsonData := make([]byte, 0)
	decBlock := make([]byte, cipher.BlockSize())

	i := 0
	for i < len(encData) {
		cipher.Decrypt(decBlock, encData[i:i+cipher.BlockSize()])
		jsonData = append(jsonData, decBlock...)
		i += cipher.BlockSize()
	}

	paddingSize := 0
	for jsonData[len(jsonData)-paddingSize-1] == 0x00 {
		paddingSize++
	}
	jsonData = RemovePadding(jsonData, paddingSize)

	err := json.Unmarshal(jsonData, data)
	if err != nil {
		return err
	}

	return nil
}
