// Code generated by protoc-gen-go. DO NOT EDIT.
// source: museum.proto

package model

import (
	context "context"
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	grpc "google.golang.org/grpc"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

type Museum struct {
	Id                   int32    `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	Name                 string   `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	Latitude             float64  `protobuf:"fixed64,3,opt,name=latitude,proto3" json:"latitude,omitempty"`
	Longitude            float64  `protobuf:"fixed64,4,opt,name=longitude,proto3" json:"longitude,omitempty"`
	Address              string   `protobuf:"bytes,5,opt,name=address,proto3" json:"address,omitempty"`
	Phone                string   `protobuf:"bytes,6,opt,name=phone,proto3" json:"phone,omitempty"`
	Contacts             string   `protobuf:"bytes,7,opt,name=contacts,proto3" json:"contacts,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *Museum) Reset()         { *m = Museum{} }
func (m *Museum) String() string { return proto.CompactTextString(m) }
func (*Museum) ProtoMessage()    {}
func (*Museum) Descriptor() ([]byte, []int) {
	return fileDescriptor_664c4e2d2a53c23f, []int{0}
}

func (m *Museum) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Museum.Unmarshal(m, b)
}
func (m *Museum) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Museum.Marshal(b, m, deterministic)
}
func (m *Museum) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Museum.Merge(m, src)
}
func (m *Museum) XXX_Size() int {
	return xxx_messageInfo_Museum.Size(m)
}
func (m *Museum) XXX_DiscardUnknown() {
	xxx_messageInfo_Museum.DiscardUnknown(m)
}

var xxx_messageInfo_Museum proto.InternalMessageInfo

func (m *Museum) GetId() int32 {
	if m != nil {
		return m.Id
	}
	return 0
}

func (m *Museum) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *Museum) GetLatitude() float64 {
	if m != nil {
		return m.Latitude
	}
	return 0
}

func (m *Museum) GetLongitude() float64 {
	if m != nil {
		return m.Longitude
	}
	return 0
}

func (m *Museum) GetAddress() string {
	if m != nil {
		return m.Address
	}
	return ""
}

func (m *Museum) GetPhone() string {
	if m != nil {
		return m.Phone
	}
	return ""
}

func (m *Museum) GetContacts() string {
	if m != nil {
		return m.Contacts
	}
	return ""
}

type GetPathRequest struct {
	MuseumIds            []int32  `protobuf:"varint,1,rep,packed,name=museumIds,proto3" json:"museumIds,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *GetPathRequest) Reset()         { *m = GetPathRequest{} }
func (m *GetPathRequest) String() string { return proto.CompactTextString(m) }
func (*GetPathRequest) ProtoMessage()    {}
func (*GetPathRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_664c4e2d2a53c23f, []int{1}
}

func (m *GetPathRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_GetPathRequest.Unmarshal(m, b)
}
func (m *GetPathRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_GetPathRequest.Marshal(b, m, deterministic)
}
func (m *GetPathRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_GetPathRequest.Merge(m, src)
}
func (m *GetPathRequest) XXX_Size() int {
	return xxx_messageInfo_GetPathRequest.Size(m)
}
func (m *GetPathRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_GetPathRequest.DiscardUnknown(m)
}

var xxx_messageInfo_GetPathRequest proto.InternalMessageInfo

func (m *GetPathRequest) GetMuseumIds() []int32 {
	if m != nil {
		return m.MuseumIds
	}
	return nil
}

type GetPathReply struct {
	Path                 []int32  `protobuf:"varint,1,rep,packed,name=path,proto3" json:"path,omitempty"`
	Distance             float64  `protobuf:"fixed64,2,opt,name=distance,proto3" json:"distance,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *GetPathReply) Reset()         { *m = GetPathReply{} }
func (m *GetPathReply) String() string { return proto.CompactTextString(m) }
func (*GetPathReply) ProtoMessage()    {}
func (*GetPathReply) Descriptor() ([]byte, []int) {
	return fileDescriptor_664c4e2d2a53c23f, []int{2}
}

func (m *GetPathReply) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_GetPathReply.Unmarshal(m, b)
}
func (m *GetPathReply) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_GetPathReply.Marshal(b, m, deterministic)
}
func (m *GetPathReply) XXX_Merge(src proto.Message) {
	xxx_messageInfo_GetPathReply.Merge(m, src)
}
func (m *GetPathReply) XXX_Size() int {
	return xxx_messageInfo_GetPathReply.Size(m)
}
func (m *GetPathReply) XXX_DiscardUnknown() {
	xxx_messageInfo_GetPathReply.DiscardUnknown(m)
}

var xxx_messageInfo_GetPathReply proto.InternalMessageInfo

func (m *GetPathReply) GetPath() []int32 {
	if m != nil {
		return m.Path
	}
	return nil
}

func (m *GetPathReply) GetDistance() float64 {
	if m != nil {
		return m.Distance
	}
	return 0
}

type GetMuseumsRequest struct {
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *GetMuseumsRequest) Reset()         { *m = GetMuseumsRequest{} }
func (m *GetMuseumsRequest) String() string { return proto.CompactTextString(m) }
func (*GetMuseumsRequest) ProtoMessage()    {}
func (*GetMuseumsRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_664c4e2d2a53c23f, []int{3}
}

func (m *GetMuseumsRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_GetMuseumsRequest.Unmarshal(m, b)
}
func (m *GetMuseumsRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_GetMuseumsRequest.Marshal(b, m, deterministic)
}
func (m *GetMuseumsRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_GetMuseumsRequest.Merge(m, src)
}
func (m *GetMuseumsRequest) XXX_Size() int {
	return xxx_messageInfo_GetMuseumsRequest.Size(m)
}
func (m *GetMuseumsRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_GetMuseumsRequest.DiscardUnknown(m)
}

var xxx_messageInfo_GetMuseumsRequest proto.InternalMessageInfo

type GetMuseumsReply struct {
	Museums              []*Museum `protobuf:"bytes,1,rep,name=museums,proto3" json:"museums,omitempty"`
	XXX_NoUnkeyedLiteral struct{}  `json:"-"`
	XXX_unrecognized     []byte    `json:"-"`
	XXX_sizecache        int32     `json:"-"`
}

func (m *GetMuseumsReply) Reset()         { *m = GetMuseumsReply{} }
func (m *GetMuseumsReply) String() string { return proto.CompactTextString(m) }
func (*GetMuseumsReply) ProtoMessage()    {}
func (*GetMuseumsReply) Descriptor() ([]byte, []int) {
	return fileDescriptor_664c4e2d2a53c23f, []int{4}
}

func (m *GetMuseumsReply) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_GetMuseumsReply.Unmarshal(m, b)
}
func (m *GetMuseumsReply) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_GetMuseumsReply.Marshal(b, m, deterministic)
}
func (m *GetMuseumsReply) XXX_Merge(src proto.Message) {
	xxx_messageInfo_GetMuseumsReply.Merge(m, src)
}
func (m *GetMuseumsReply) XXX_Size() int {
	return xxx_messageInfo_GetMuseumsReply.Size(m)
}
func (m *GetMuseumsReply) XXX_DiscardUnknown() {
	xxx_messageInfo_GetMuseumsReply.DiscardUnknown(m)
}

var xxx_messageInfo_GetMuseumsReply proto.InternalMessageInfo

func (m *GetMuseumsReply) GetMuseums() []*Museum {
	if m != nil {
		return m.Museums
	}
	return nil
}

func init() {
	proto.RegisterType((*Museum)(nil), "model.Museum")
	proto.RegisterType((*GetPathRequest)(nil), "model.GetPathRequest")
	proto.RegisterType((*GetPathReply)(nil), "model.GetPathReply")
	proto.RegisterType((*GetMuseumsRequest)(nil), "model.GetMuseumsRequest")
	proto.RegisterType((*GetMuseumsReply)(nil), "model.GetMuseumsReply")
}

func init() { proto.RegisterFile("museum.proto", fileDescriptor_664c4e2d2a53c23f) }

var fileDescriptor_664c4e2d2a53c23f = []byte{
	// 337 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x9c, 0x52, 0x3d, 0x4f, 0xc3, 0x30,
	0x14, 0xc4, 0x6d, 0xd3, 0xd0, 0x47, 0x29, 0xe0, 0x02, 0xb2, 0x2a, 0x86, 0x28, 0x0b, 0x99, 0x32,
	0x94, 0x01, 0x89, 0x81, 0x01, 0x41, 0x2b, 0x06, 0x24, 0x94, 0x7f, 0x60, 0xe2, 0x27, 0x12, 0x29,
	0x5f, 0xc4, 0x2f, 0x43, 0x7f, 0x05, 0xff, 0x85, 0x5f, 0x88, 0x62, 0xa7, 0x29, 0x45, 0x4c, 0x6c,
	0xbe, 0xbb, 0xe7, 0x77, 0x77, 0x96, 0x61, 0x9a, 0x37, 0x1a, 0x9b, 0x3c, 0xac, 0xea, 0x92, 0x4a,
	0xee, 0xe4, 0xa5, 0xc2, 0xcc, 0xff, 0x62, 0x30, 0x7e, 0x31, 0x3c, 0x9f, 0xc1, 0x20, 0x55, 0x82,
	0x79, 0x2c, 0x70, 0xa2, 0x41, 0xaa, 0x38, 0x87, 0x51, 0x21, 0x73, 0x14, 0x03, 0x8f, 0x05, 0x93,
	0xc8, 0x9c, 0xf9, 0x02, 0x0e, 0x33, 0x49, 0x29, 0x35, 0x0a, 0xc5, 0xd0, 0x63, 0x01, 0x8b, 0x7a,
	0xcc, 0xaf, 0x60, 0x92, 0x95, 0xc5, 0xbb, 0x15, 0x47, 0x46, 0xdc, 0x11, 0x5c, 0x80, 0x2b, 0x95,
	0xaa, 0x51, 0x6b, 0xe1, 0x98, 0x85, 0x5b, 0xc8, 0xcf, 0xc1, 0xa9, 0x92, 0xb2, 0x40, 0x31, 0x36,
	0xbc, 0x05, 0xad, 0x53, 0x5c, 0x16, 0x24, 0x63, 0xd2, 0xc2, 0x35, 0x42, 0x8f, 0xfd, 0x10, 0x66,
	0x6b, 0xa4, 0x57, 0x49, 0x49, 0x84, 0x1f, 0x0d, 0x6a, 0x6a, 0xbd, 0x6d, 0xbb, 0x67, 0xa5, 0x05,
	0xf3, 0x86, 0x81, 0x13, 0xed, 0x08, 0xff, 0x1e, 0xa6, 0xfd, 0x7c, 0x95, 0x6d, 0xda, 0x66, 0x95,
	0xa4, 0xa4, 0x1b, 0x34, 0xe7, 0xd6, 0x4f, 0xa5, 0x9a, 0x64, 0x11, 0xdb, 0xc6, 0x2c, 0xea, 0xb1,
	0x3f, 0x87, 0xb3, 0x35, 0x92, 0x7d, 0x26, 0xdd, 0x59, 0xfa, 0x77, 0x70, 0xf2, 0x93, 0x6c, 0xf7,
	0x5e, 0x83, 0x6b, 0x4d, 0x6d, 0x86, 0xa3, 0xe5, 0x71, 0x68, 0x5e, 0x39, 0xb4, 0x53, 0xd1, 0x56,
	0x5d, 0x3e, 0x01, 0xb4, 0x69, 0x56, 0x69, 0xa1, 0xb0, 0xe6, 0xb7, 0xe0, 0x76, 0xf1, 0xf8, 0x45,
	0x77, 0x61, 0xbf, 0xde, 0x62, 0xfe, 0x9b, 0xae, 0xb2, 0x8d, 0x7f, 0xb0, 0xfc, 0x64, 0x70, 0x6a,
	0x57, 0x3f, 0xa6, 0xba, 0x92, 0x14, 0x27, 0x58, 0xf3, 0x07, 0x98, 0xae, 0x90, 0xe2, 0xa4, 0x4b,
	0xc6, 0xc5, 0xee, 0xee, 0x7e, 0x83, 0xc5, 0xe5, 0x1f, 0x8a, 0x59, 0xfc, 0xef, 0x44, 0x6f, 0x63,
	0xf3, 0xb9, 0x6e, 0xbe, 0x03, 0x00, 0x00, 0xff, 0xff, 0x04, 0x79, 0x07, 0xa9, 0x6c, 0x02, 0x00,
	0x00,
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// PathFinderClient is the client API for PathFinder service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type PathFinderClient interface {
	GetPath(ctx context.Context, in *GetPathRequest, opts ...grpc.CallOption) (*GetPathReply, error)
}

type pathFinderClient struct {
	cc *grpc.ClientConn
}

func NewPathFinderClient(cc *grpc.ClientConn) PathFinderClient {
	return &pathFinderClient{cc}
}

func (c *pathFinderClient) GetPath(ctx context.Context, in *GetPathRequest, opts ...grpc.CallOption) (*GetPathReply, error) {
	out := new(GetPathReply)
	err := c.cc.Invoke(ctx, "/model.PathFinder/GetPath", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// PathFinderServer is the server API for PathFinder service.
type PathFinderServer interface {
	GetPath(context.Context, *GetPathRequest) (*GetPathReply, error)
}

func RegisterPathFinderServer(s *grpc.Server, srv PathFinderServer) {
	s.RegisterService(&_PathFinder_serviceDesc, srv)
}

func _PathFinder_GetPath_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetPathRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(PathFinderServer).GetPath(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/model.PathFinder/GetPath",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(PathFinderServer).GetPath(ctx, req.(*GetPathRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _PathFinder_serviceDesc = grpc.ServiceDesc{
	ServiceName: "model.PathFinder",
	HandlerType: (*PathFinderServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "GetPath",
			Handler:    _PathFinder_GetPath_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "museum.proto",
}

// MuseumDispatcherClient is the client API for MuseumDispatcher service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type MuseumDispatcherClient interface {
	FetchMuseums(ctx context.Context, in *GetMuseumsRequest, opts ...grpc.CallOption) (*GetMuseumsReply, error)
	GetPath(ctx context.Context, in *GetPathRequest, opts ...grpc.CallOption) (*GetPathReply, error)
}

type museumDispatcherClient struct {
	cc *grpc.ClientConn
}

func NewMuseumDispatcherClient(cc *grpc.ClientConn) MuseumDispatcherClient {
	return &museumDispatcherClient{cc}
}

func (c *museumDispatcherClient) FetchMuseums(ctx context.Context, in *GetMuseumsRequest, opts ...grpc.CallOption) (*GetMuseumsReply, error) {
	out := new(GetMuseumsReply)
	err := c.cc.Invoke(ctx, "/model.MuseumDispatcher/FetchMuseums", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *museumDispatcherClient) GetPath(ctx context.Context, in *GetPathRequest, opts ...grpc.CallOption) (*GetPathReply, error) {
	out := new(GetPathReply)
	err := c.cc.Invoke(ctx, "/model.MuseumDispatcher/GetPath", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// MuseumDispatcherServer is the server API for MuseumDispatcher service.
type MuseumDispatcherServer interface {
	FetchMuseums(context.Context, *GetMuseumsRequest) (*GetMuseumsReply, error)
	GetPath(context.Context, *GetPathRequest) (*GetPathReply, error)
}

func RegisterMuseumDispatcherServer(s *grpc.Server, srv MuseumDispatcherServer) {
	s.RegisterService(&_MuseumDispatcher_serviceDesc, srv)
}

func _MuseumDispatcher_FetchMuseums_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetMuseumsRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MuseumDispatcherServer).FetchMuseums(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/model.MuseumDispatcher/FetchMuseums",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MuseumDispatcherServer).FetchMuseums(ctx, req.(*GetMuseumsRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _MuseumDispatcher_GetPath_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetPathRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MuseumDispatcherServer).GetPath(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/model.MuseumDispatcher/GetPath",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MuseumDispatcherServer).GetPath(ctx, req.(*GetPathRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _MuseumDispatcher_serviceDesc = grpc.ServiceDesc{
	ServiceName: "model.MuseumDispatcher",
	HandlerType: (*MuseumDispatcherServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "FetchMuseums",
			Handler:    _MuseumDispatcher_FetchMuseums_Handler,
		},
		{
			MethodName: "GetPath",
			Handler:    _MuseumDispatcher_GetPath_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "museum.proto",
}
